Form Display Actions module


INTRODUCTION
=============

The Form Display Actions module adds in an actions column to the form display which allows you directly act (e.g. edit, delete) on a field without having to go back to the manage fields page. 


REQUIREMENTS
============

No special requirements.


INSTALLATION
============

Install as you would normally install a contributed Drupal module. See: https://www.drupal.org/documentation/install/modules-themes/modules-8


MAINTAINERS
===========

Current Maintainers:
* Michael Welford - https://www.drupal.org/u/mikejw

This project has been sponsored by:
* The University of Adelaide - https://www.drupal.org/university-of-adelaide
  Visit: http://www.adelaide.edu.au
